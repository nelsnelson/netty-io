#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'java'
require 'netty'

# rubocop: disable all
module Example
  java_import Java::io.netty.bootstrap.ServerBootstrap
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.channel.ChannelInboundHandlerAdapter
  java_import Java::io.netty.channel.ChannelInitializer
  java_import Java::io.netty.channel.ChannelOption
  java_import Java::io.netty.channel.ChannelPipeline
  java_import Java::io.netty.channel.EventLoopGroup
  java_import Java::io.netty.channel.nio.NioEventLoopGroup
  java_import Java::io.netty.channel.socket.nio.NioServerSocketChannel
  java_import Java::io.netty.handler.codec.DelimiterBasedFrameDecoder
  java_import Java::io.netty.handler.codec.Delimiters
  java_import Java::io.netty.handler.codec.string.StringDecoder
  java_import Java::io.netty.handler.codec.string.StringEncoder
  java_import Java::io.netty.handler.logging.LogLevel
  java_import Java::io.netty.handler.logging.LoggingHandler

  class EchoServer
    PORT = 8007
    def initialize()
      @boss_group = NioEventLoopGroup.new(1)
      @worker_group = NioEventLoopGroup.new
      @bootstrap = ServerBootstrap.new
        .group(@boss_group, @worker_group)
        .channel(NioServerSocketChannel.java_class)
        .childHandler(ChannelInitializerExample.new)
        # .option(ChannelOption::SO_BACKLOG, 100.to_java(java.lang.Integer))
    end
    def run
      f = @bootstrap.bind(PORT.to_i).sync()
      f.channel.closeFuture().sync()
    ensure
      @boss_group.shutdownGracefully()
      @worker_group.shutdownGracefully()
    end
  end
  class EchoServerHandlerExample < ChannelInboundHandlerAdapter
    # def isSharable()
    #   true
    # end
    def channelRead(ctx, msg)
      if msg == 'quit'
        future = ctx.channel().close()
        future.addListener(ChannelFutureListener::CLOSE)
        ctx.channel().parent().close()
      else
        ctx.channel.writeAndFlush("#{msg}\n")
      end
    end
    def channelReadComplete(ctx)
      # ctx.flush()
    end
    def exceptionCaught(ctx, cause)
      cause.printStackTrace() unless cause.is_a?(SystemExit)
      ctx.close()
    end
  end
  class ChannelInitializerExample < ChannelInitializer
    def initChannel(ch)
      pipeline = ch.pipeline()
      pipeline.addLast(DelimiterBasedFrameDecoder.new(8192, Delimiters.lineDelimiter))
      pipeline.addLast(StringDecoder.new)
      pipeline.addLast(StringEncoder.new)
      pipeline.addLast(EchoServerHandlerExample.new)
    end
  end

  def main(args = ARGV)
    EchoServer.new.run
  end
end

Object.new.extend(Example).main if $PROGRAM_NAME == __FILE__
# rubocop: enable all
