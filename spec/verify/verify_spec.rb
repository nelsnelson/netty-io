# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

unless defined?(PROJECT)
  VERIFY_DIR_PATH = File.expand_path(__dir__) unless defined?(VERIFY_DIR_PATH)
  SPEC_DIR_PATH = File.expand_path(File.dirname(VERIFY_DIR_PATH)) unless defined?(SPEC_DIR_PATH)
  PROJECT_DIR_PATH = File.expand_path(File.dirname(SPEC_DIR_PATH)) unless defined?(PROJECT_DIR_PATH)
  PROJECT = File.basename(PROJECT_DIR_PATH)
end

RSpec.describe 'verify gem' do
  it 'should support an echo server' do
    require 'fileutils'
    require 'socket'
    load "#{PROJECT}.gemspec"
    spec_verify_path = File.expand_path(File.dirname(__FILE__))
    spec_path = File.expand_path(File.dirname(spec_verify_path))
    project_path = File.expand_path(File.dirname(spec_path))
    gem_spec_full_name = GEM_SPEC.full_name
    gem_file_name = gem_spec_full_name + '.gem'
    gem_path = File.join(project_path, gem_file_name)
    FileUtils.mkdir_p 'tmp'
    system('jgem', 'install', '--no-document', '--install-dir=tmp', gem_path)
    gem_lib_dir_path = File.join('tmp', 'gems', gem_spec_full_name, 'lib')
    verify_rb_file_path = File.join('spec', 'verify', 'example.rb')
    begin
      cmd = "jruby -I#{gem_lib_dir_path} #{verify_rb_file_path}"
      puts "Executing command: #{cmd}"
      process = java.lang.Runtime.getRuntime().exec(cmd)
      timeout = 10
      start = Time.now
      results = ''
      begin
        socket = TCPSocket.new('localhost', 8007)
        socket.puts "Hello world!"
        while line = socket.gets
          results << line.chop
          socket.puts 'quit'
        end
      rescue Errno::ECONNREFUSED => e
        STDERR.puts e.message
        sleep 1
        retry unless (Time.now - start) > timeout
      rescue StandardError => e
        STDERR.puts "Unexpected error: #{e.class} #{e.message}"
      end
      expected_results = 'Hello world!'
      expect(results).to eq(expected_results)
    rescue StandardError => e
      STDERR.puts "Unexpected error: #{e.class} #{e.message}"
    ensure
      begin
        socket.close unless socket.nil?
        process.destroy()
        process.destroyForcibly() if process.isAlive()
      rescue StandardError => e
        STDERR.puts "Unexpected error: #{e.class} #{e.message}"
      end
    end
  end
end
