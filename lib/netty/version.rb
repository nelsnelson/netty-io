# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

module Netty
  def self.version
    File.read('version').chomp
  end

  def self.release
    File.read('release').chomp
  end

  def self.release_version(release = Netty.release)
    release_version = [Netty.version]
    release_version << release unless release.nil?
    release_version.join('.').freeze
  end

  VERSION = Netty.version
  GEM_RELEASE = Netty.release
end
