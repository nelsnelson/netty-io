# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require_relative 'lib/netty/version'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)

GEM_SPEC_FILES = -> {
  %w[LICENSE.txt Rakefile README.md] + [
    Dir['*.gemspec'],
    Dir[File.join('lib', '**', '*.rb')],
    Dir[File.join('lib', '**', '*.jar')]
  ].flatten
} unless defined?(GEM_SPEC_FILES)

# rubocop: disable Metrics/AbcSize
# rubocop: disable Metrics/MethodLength
def gem_spec
  Gem::Specification.new do |spec|
    spec.name = PROJECT
    spec.version = Netty.release_version
    spec.summary = 'The netty.io Netty java jars packaged as a gem.'
    spec.description =
      'Netty is an asynchronous event-driven network ' \
      'application framework for rapid development of maintainable high ' \
      'performance protocol servers & clients.'
    spec.authors = ['Nels Nelson']
    spec.email = 'nels@nelsnelson.org'
    spec.files = GEM_SPEC_FILES.call
    spec.extensions = Dir[File.join('ext', '**', 'extconf.rb')]
    spec.platform = 'java'
    spec.homepage = "https://rubygems.org/gems/#{PROJECT}"
    spec.metadata = {
      'source_code_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}",
      'bug_tracker_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}/issues",
      'canonical_pom_uri' => 'https://repo1.maven.org/maven2/io/netty/netty-all/' \
        "#{Netty::VERSION}.Final/netty-all-#{Netty::VERSION}.Final.pom",
    }
    spec.license = 'Apache-2.0'

    spec.required_ruby_version = '>= 2.5.3'
    spec.add_development_dependency 'rake', '~> 13.0.6'
    spec.add_development_dependency 'rspec', '~> 3.11.0'
    spec.add_development_dependency 'rubocop', '~> 1.25.0'
  end
end
# rubocop: enable Metrics/AbcSize
# rubocop: enable Metrics/MethodLength

GEM_SPEC = gem_spec unless defined?(GEM_SPEC)

GEM_SPEC
