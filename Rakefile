# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'digest'
require 'rake'
require 'rake/clean'

load "#{File.basename(__dir__)}.gemspec"

CLEAN.add File.join('lib', '**', '*.jar'), File.join('tmp', '**', '*'), 'tmp'
CLOBBER.add '*.gem', File.join('ext', 'apache-maven-*'), File.join('ext', '.m2'), 'pkg'

desc 'Resolve dependencies'
task :dependencies do
  # Include the extension configurator
  GEM_SPEC.extensions.each { |path| require_relative path }
  # Avoid invocation of the extension configurator upon user installation of gem
  GEM_SPEC.extensions.clear
  jar_paths = jars(File.join('io', 'netty'))
  FileUtils.cp(jar_paths, File.join('lib', 'netty'))
end

desc 'Assemble the gem package'
task :package do
  require 'fileutils'
  GEM_SPEC.files = GEM_SPEC_FILES.call if defined?(GEM_SPEC_FILES)
  gem_spec_name = GEM_SPEC.name
  gem_spec_full_name = GEM_SPEC.full_name
  package_dir_path = File.join('pkg', gem_spec_full_name)
  FileUtils.mkdir_p(package_dir_path)
  GEM_SPEC.files.each do |path|
    FileUtils.mkdir_p(File.join(package_dir_path, File.dirname(path)))
    FileUtils.cp_r(path, File.join(package_dir_path, path))
  end
  gem_spec_file_name = "#{gem_spec_name}.gemspec"
  package_gemspec_file_path = File.join(package_dir_path, gem_spec_file_name)
  File.open(package_gemspec_file_path, 'w') do |io|
    io.write(GEM_SPEC.to_ruby)
  end
end
task package: [:clobber, :dependencies]

desc 'Build the gem from the assembled package'
task :gem do
  # Reload the gem spec version
  GEM_SPEC.version = Netty::release_version
  gem_spec_full_name = GEM_SPEC.full_name
  package_dir_path = File.join('pkg', gem_spec_full_name)
  Dir.chdir(package_dir_path) do
    system('jgem', 'build')
  end
  gem_file_name = "#{gem_spec_full_name}.gem"
  gem_file_path = File.join(package_dir_path, gem_file_name)
  project_path = File.expand_path(__dir__)
  FileUtils.mv(gem_file_path, project_path) if File.exists?(gem_file_path)
  Rake::Task[:verify]&.invoke
end
task gem: [:spec, :package]

# Unit tests
begin
  require 'rspec/core'
  require 'rspec/core/rake_task'
  RSpec.configure do |config|
    config.libs << 'lib' << 'spec'
  end

  desc 'Run unit tests'
  RSpec::Core::RakeTask.new(:spec) do |task|
    task.pattern = File.join('spec', 'rake', '*_spec.rb')
  end

  desc 'Run coverage'
  RSpec::Core::RakeTask.new(:rcov) do |task|
    task.rcov = true
    task.pattern = File.join('spec', 'rake', '*_spec.rb')
  end

  desc 'Run gem verification tests'
  RSpec::Core::RakeTask.new(:verify) do |task|
    project_path = File.expand_path(__dir__)
    gem_spec_full_name = GEM_SPEC.full_name
    gem_file_name = "#{gem_spec_full_name}.gem"
    gem_path = File.join(project_path, gem_file_name)
    ENV['GEM_PATH'] = gem_path
    ENV['GEM_SPEC_FULL_NAME'] = gem_spec_full_name
    Rake::Task[:gem].invoke unless File.exists? gem_path
    task.pattern = File.join('spec', 'verify', '*_spec.rb')
  end
rescue LoadError
  # no rspec available
  puts "To install rspec run: jgem install rspec"
rescue StandardError => e
  puts "Unexpected error: #{e.message}"
end

desc 'Upload gem to https://rubygems.org/'
task :publish do
  project_path = File.expand_path(__dir__)
  gem_spec_full_name = GEM_SPEC.full_name
  gem_file_name = "#{gem_spec_full_name}.gem"
  gem_path = File.join(project_path, gem_file_name)
  Rake::Task[:gem].invoke unless File.exists? gem_path
  system('jgem', 'push', gem_path)
end
task publish: :verify

task default: :gem
