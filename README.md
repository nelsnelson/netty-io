# netty-io

[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg?style=flat)][license]

This packages the [netty.io Netty] jars into a convenient gem for requiring.

```rb
require 'netty-io'
```


## Setup

To setup the development environment for building the gem, some dependencies must be installed.

### Install asdf

The [asdf] CLI tool used to manage multiple runtime versions.

```sh
git clone https://github.com/asdf-vm/asdf.git "${HOME}/.asdf"
pushd "${HOME}/.asdf"; git fetch origin; popd
source "${HOME}/.asdf/asdf.sh"; source "${HOME}/.asdf/completions/asdf.bash"
```


### Install required runtime software

Download and install the latest version of the [Java JDK].

```sh
asdf plugin add java
asdf install java openjdk-13.0.2
```


Download and install the latest version of [JRuby].

```sh
asdf plugin add ruby
pushd "${HOME}/.asdf/plugins/ruby/ruby-build"; git fetch origin; git pull origin master; popd
asdf list all ruby > /dev/null
asdf install ruby jruby-9.3.2.0
```


### Install required gems

Use bundler to install the required gem dependencies.

```sh
bundle install
```


## Building

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```sh
bundle exec rake clobber package spec gem verify
```


## Publish

To publish the gem, execute:

```sh
bundle exec rake publish
```

[license]: https://gitlab.com/nelsnelson/apache-log4j-2/blob/main/LICENSE.txt
[asdf]: https://asdf-vm.com/
[Java JDK]: https://www.java.com/en/download/
[JRuby]: https://jruby.org/download
[netty.io Netty]: https://netty.io/
